//! Traits that define the core operations needed to support the union-find algorithm.
//!
//! The trait [`UnionFind`](crate::traits::UnionFind) defines the 3 main operations of union-find:
//! 1. `find_set`
//! 1. `make_set`
//! 1. `union`
//!
//! # Algorithmic Complexity
//!
//! The algorithmic complexity of each of these operations will depend on the specific
//! implementation of this trait.
//!
//! Unless you have specific requirements, you should use [`DisjointSets`](crate::disjoint_sets::DisjointSets)
//! as it combines _union-by-rank_ and _path-compression_ heuristics to acheieve the most optimal
//! algorithmic complexity.

use std::hash::Hash;

/// Defines the minimal set of operations to support the union-find algorithm.
///
/// These functions handle the item type `T` directly, in order to abstract the internal
/// architecture of the implementing structs.
///
/// Union-find, as defined by this trait, handles types `T` that implement `Copy`, `Eq` and `Hash`.
/// This implies that you can use it with:
/// 1. trivial types like `bool`, `i32`, `usize` and etc.
/// 1. any struct that implement `Copy`, `Eq` and `Hash`
/// 1. references to structs that implement `Eq` and `Hash`
///     1. this is because all references `&T` are copyable, and they use the `Eq` and `Hash`
///        implementations of the underlying type `T`
///
/// # Examples
///
/// Trivial primitive types work well with `DisjointSets`. Take `usize` for example: the following
/// code compiles.
/// ```
/// use union_find_rs::prelude::*;
///
/// let mut sets: DisjointSets<usize> = DisjointSets::new();
/// ```
///
/// More generally, `DisjointSets` works with types that implement the traits `Copy`, `Eq`, and
/// `Hash`:
/// ```
/// use union_find_rs::prelude::*;
/// use std::hash::Hash;
///
/// #[derive(Copy, Clone, PartialEq, Eq, Hash)]
/// struct CustomStruct {}
///
/// let mut sets: DisjointSets<CustomStruct> = DisjointSets::new();
/// ```
///
/// You may have structs that implement `Eq` and `Hash`, but not `Copy`. This is fairly common
/// because non-trivial data structures are often don't implement `Copy`. For example, the
/// following code does not compile:
/// ```compile_fail
/// use union_find_rs::prelude::*;
/// use std::hash::Hash;
///
/// #[derive(PartialEq, Eq, Hash)]
/// struct CustomStruct {}
///
/// let mut sets: DisjointSets<CustomStruct> = DisjointSets::new();
/// ```
///
/// In these cases, use references:
/// ```
/// use union_find_rs::prelude::*;
/// use std::hash::Hash;
///
/// #[derive(PartialEq, Eq, Hash)]
/// struct CustomStruct {}
///
/// let mut sets: DisjointSets<&CustomStruct> = DisjointSets::new();
/// ```
pub trait UnionFind<T>
where
    T: Copy + Eq + Hash,
{
    /// Find the representative of the set that contains `item`, if any.
    ///
    /// If there is no set that contains `item`, an `Err` with an appropriate variant of
    /// [`Error`](crate::traits::Error) will be returned.
    ///
    /// Guarantees that:
    /// 1. if `x == y`, then `find_set(x) == find_set(y)`
    /// 1. if `x` and `y` are in the same set, then `find_set(x) == find_set(y)`
    /// 1. if `x` and `y` are not in the same set, then `find_set(x) != find_set(y)`
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    /// use std::collections::HashSet;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    ///
    /// sets.make_set(1).unwrap();
    /// sets.make_set(4).unwrap();
    /// sets.make_set(9).unwrap();
    ///
    /// sets.union(&1, &4).unwrap();
    ///
    /// // equivalent items have the same representative
    /// assert_eq!(sets.find_set(&1).unwrap(), sets.find_set(&1).unwrap());
    /// assert_eq!(sets.find_set(&4).unwrap(), sets.find_set(&4).unwrap());
    /// assert_eq!(sets.find_set(&9).unwrap(), sets.find_set(&9).unwrap());
    ///
    /// // items in the same set have the same representative
    /// assert_eq!(sets.find_set(&1).unwrap(), sets.find_set(&4).unwrap());
    ///
    /// // items in different sets have different representatives
    /// assert_ne!(sets.find_set(&1).unwrap(), sets.find_set(&9).unwrap());
    /// assert_ne!(sets.find_set(&4).unwrap(), sets.find_set(&9).unwrap());
    /// ```
    fn find_set(&self, item: &T) -> Result<T>;

    /// Create a new set that contains only `item` if `item` does not exist in any of the disjoint
    /// sets.
    ///
    /// If there is a set that contains `item`, an `Err` with an appropriate variant of
    /// [`Error`](crate::traits::Error) will be returned.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    /// use std::collections::HashSet;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    ///
    /// sets.make_set(4).unwrap();
    /// sets.make_set(9).unwrap();
    ///
    /// // the disjoint sets as a vector of sets
    /// let as_vec: Vec<HashSet<usize>> = sets.into_iter().collect();
    ///
    /// // there should be 2 disjoint sets, where one of them only contains `4` and the other one
    /// // only contains `9`
    /// assert_eq!(as_vec.len(), 2);
    /// assert!(
    ///     as_vec
    ///         .iter()
    ///         .any(|set| set == &vec![4].into_iter().collect::<HashSet<usize>>())
    /// );
    /// assert!(
    ///     as_vec
    ///         .iter()
    ///         .any(|set| set == &vec![9].into_iter().collect::<HashSet<usize>>())
    /// );
    /// ```
    fn make_set(&mut self, item: T) -> Result<()>;

    /// Merge the set that contains `x` and the set that contains `y`.
    ///
    /// If either `x` or `y` or any of their parents does not exist, an `Err` with an
    /// appropriate variant of [`Error`](crate::traits::Error) will be returned.
    ///
    /// # Example
    ///
    /// ```
    /// use union_find_rs::prelude::*;
    /// use std::collections::HashSet;
    ///
    /// let mut sets: DisjointSets<usize> = DisjointSets::new();
    ///
    /// sets.make_set(1).unwrap();
    /// sets.make_set(4).unwrap();
    /// sets.make_set(9).unwrap();
    ///
    /// sets.union(&1, &4).unwrap();
    ///
    /// // the disjoint sets as a vector of sets
    /// let as_vec: Vec<HashSet<usize>> = sets.into_iter().collect();
    ///
    /// // there should be 2 disjoint sets, where one of them only contains `9` and the other one
    /// // only contains `1` and `4`
    /// assert_eq!(as_vec.len(), 2);
    /// assert!(
    ///     as_vec
    ///         .iter()
    ///         .any(|set| set == &vec![9].into_iter().collect::<HashSet<usize>>())
    /// );
    /// assert!(
    ///     as_vec
    ///         .iter()
    ///         .any(|set| set == &vec![1, 4].into_iter().collect::<HashSet<usize>>())
    /// );
    /// ```
    fn union(&mut self, x: &T, y: &T) -> Result<()>;
}

//
pub type Result<T> = std::result::Result<T, Error>;

/// Different failure modes for the union-find operation.
#[derive(Debug)]
pub enum Error {
    /// Indicates that an item already exists in one of the disjoint sets.
    ItemAlreadyExists,

    /// Indicates that an item does not exist in any of the disjoint sets.
    ItemDoesNotExist,
}
