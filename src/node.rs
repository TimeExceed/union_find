use std::cell::Cell;

// nodes in the disjoint set forest of `DisjointSets`
//
// the only requirement is that the item that it holds is copyable
#[derive(Debug, Eq, PartialEq)]
pub struct Node<T>
where
    T: Copy,
{
    //
    item: T,
    // using `Cell` here for interior mutability that is needed during `DisjointSets::find_set`
    parent: Cell<Option<T>>,
    rank: Cell<usize>,
}

//
impl<T> Node<T>
where
    T: Copy,
{
    //
    pub fn new(item: T, rank: usize) -> Self {
        Self {
            item,
            parent: Cell::new(None),
            rank: Cell::new(rank),
        }
    }

    //
    pub fn get_item(&self) -> T {
        self.item
    }

    //
    pub fn get_parent(&self) -> Option<T> {
        self.parent.get()
    }
    pub fn set_parent<U>(&self, parent: &U)
    where
        U: AsRef<T>,
    {
        self.parent.set(Some(*parent.as_ref()));
    }

    //
    pub fn get_rank(&self) -> usize {
        self.rank.get()
    }
    pub fn set_rank(&self, rank: usize) {
        self.rank.set(rank);
    }
}

//
impl<T> AsRef<T> for Node<T>
where
    T: Copy,
{
    fn as_ref(&self) -> &T {
        &self.item
    }
}
